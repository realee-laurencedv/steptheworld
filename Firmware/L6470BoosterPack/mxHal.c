/*
 * mxHal.c
 *
 *	Created on: Mar 21, 2015
 *	Author:		Laurence DV
 *	Note:		Project specific function and variables
 */
/*--------------------------------- +
|	Include							|
+ ---------------------------------*/
#include "mxHal.h"

/*--------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
uint16_t heapAvailable = 1024;				//Heap size given by the linker
#warning	"Ensure linker option (-heap_size) are the same as heapAvailable"

/*--------------------------------- +
|	Function						|
+ ---------------------------------*/
void init(void)
{
	WDT_A_hold(WDT_A_BASE);					//Disable the Watchdog timer

	// ==== IO ==== //
	// Init all io here
	#ifdef __MSP430_HAS_PORT1_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P1, IOM_LED_GREEN_PIN | IOM_P1_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P1, IOM_LED_GREEN_PIN | IOM_P1_UNUSED_PIN);

		GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, IOM_BTN1_PIN);
		GPIO_enableInterrupt(		GPIO_PORT_P1, IOM_BTN1_PIN);
		GPIO_interruptEdgeSelect(	GPIO_PORT_P1, IOM_BTN1_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
		GPIO_clearInterruptFlag(	GPIO_PORT_P1, IOM_BTN1_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT2_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P2, IOM_P2_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P2, IOM_P2_UNUSED_PIN);

		GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P2, IOM_BTN2_PIN);
		GPIO_enableInterrupt(		GPIO_PORT_P2, IOM_BTN2_PIN);
		GPIO_interruptEdgeSelect(	GPIO_PORT_P2, IOM_BTN2_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
		GPIO_clearInterruptFlag(	GPIO_PORT_P2, IOM_BTN2_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT3_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P3, IOM_P3_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P3, IOM_P3_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT4_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P4, IOM_LED_RED_PIN | IOM_P4_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P4, IOM_LED_RED_PIN | IOM_P4_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT5_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P5, IOM_P5_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P5, IOM_P5_UNUSED_PIN);

		GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P5, IOM_XT2IN_PIN);
		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P5, IOM_XT2OUT_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT6_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P6, IOM_P6_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P6, IOM_P6_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT7_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P7, IOM_P7_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P7, IOM_P7_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT8_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P8, IOM_P8_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P8, IOM_P8_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORT9_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_P9, IOM_P9_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_P9, IOM_P9_UNUSED_PIN);
	#endif

	#ifdef __MSP430_HAS_PORTJ_R__
		GPIO_setOutputLowOnPin(	GPIO_PORT_PJ, IOM_PJ_UNUSED_PIN);
		GPIO_setAsOutputPin(	GPIO_PORT_PJ, IOM_PJ_UNUSED_PIN);
	#endif
	// ============ //

	// ==== Clock ==== //
	if(!PMM_setVCore(PMM_CORE_LEVEL_3))		//Minimum power level for USB is 2 and for 25MHz operation it's 3
		__no_operation();					//STATUS_SUCCESS = 1 in TI driverlib
	UCS_setExternalClockSource(XTAL_FREQ_XT1_HZ, XTAL_FREQ_XT2_HZ);		//Tell the system what is the freq of the crystals

	// Start the XT2
	if (!UCS_XT2StartWithTimeout(UCS_XT2DRIVE_4MHZ_8MHZ, 1000))			//XT2 will be our main clock source
			__no_operation();

	// Start the DCO/FLL
	UCS_clockSignalInit(UCS_FLLREF, UCS_XT2CLK_SELECT, UCS_CLOCK_DIVIDER_16);
	UCS_initFLLSettle(CLOCK_SYSTEM_HZ/1000, CLOCK_SYSTEM_HZ/(XTAL_FREQ_XT2_HZ/16));

	// Configure clocks
	UCS_clockSignalInit(UCS_ACLK, UCS_REFOCLK_SELECT, UCS_CLOCK_DIVIDER_1);
	UCS_clockSignalInit(UCS_SMCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);
	// =============== //

	// ==== Peripherals ==== //
	// Init peripheral lib here

	UsciAUartLibInit();
	// ===================== //

	// ==== Interrupt ==== //
	// Configure and enable interrupt not controlled by the peripheral libs
	__enable_interrupt();
	// =================== //
}

