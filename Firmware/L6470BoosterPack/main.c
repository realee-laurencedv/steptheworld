/*--------------------------------- +
|	Include							|
+ ---------------------------------*/
#include "driverlib.h"
#include "mxHal.h"
#include "Include/mxStdDef.h"
#include "Include/lhf.h"
#include "HardPeripheral/UsciAUart.h"

/*--------------------------------- +
|	Define							|
+ ---------------------------------*/
#define BUFFER_SIZE		128

/*--------------------------------- +
|	Type							|
+ ---------------------------------*/

/*--------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
struct{
		uint8_t testPrint;
		struct{
			UsciAUartHandle_t *	USCIA0;
		}hardware;
}control = {	.testPrint = 0,
				.hardware.USCIA0 = NULL};

/*--------------------------------- +
|	Prototype						|
+ ---------------------------------*/


/*================================= +
|	Main							|
+ =================================*/
int main(void) {
	// ==== Init ==== // <----- Could go in the init() function of mxHal.c
	init();

	// ============== //

	// ==== Hardware Object ==== //
	control.hardware.USCIA0 = UsciAUartCreate(USCI_A0_BASE, BUFFER_SIZE);
	P4OUT |= BIT0;
	// ========================= //

	// ==== Loop ==== //
	while (1)
	{
		_delay_cycles(10000);
		UsciAUartSend(control.hardware.USCIA0, &(control.testPrint), 1);
		control.testPrint++;
	}
	// ============== //
	return (0);
}

/*--------------------------------- +
|	Function						|
+ ---------------------------------*/


/*================================= +
|	Interrupt Vector				|
+ =================================*/
// ======= Port 1 ======== //
#pragma vector = PORT1_VECTOR
__interrupt void PORT1ISR(void)
{
	switch (P1IV)
	{
		case PEM_BTN1_INTMASK:
			break;
	}
}

// ======= Port 2 ======== //
#pragma vector = PORT2_VECTOR
__interrupt void PORT2ISR(void)
{
	switch (P2IV)
	{
		case PEM_BTN2_INTMASK:
			break;
	}
}

// ======= Serial 0 ======== //
#pragma vector = USCI_A0_VECTOR
__interrupt void USCIA0ISR(void)
{
	UsciAUartISR(control.hardware.USCIA0);
}

// ======= Catch-all ====== //
#pragma vector=unused_interrupts
__interrupt void user_trap_function(void)
{
	__no_operation();		//Put a breakpoint here!

    // code for handling all interrupts that do not have
    // specific ISRs
}
